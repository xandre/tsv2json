#!/usr/bin/env python3
import click
import pandas as pd
import json


def read_tsv(file_name):
    df = pd.read_csv(file_name, sep='\t', header=0)

    return df


def process_data(data, base_url, start_date):
    """
    Transform and clean received TSV data
    """
    # Data preparation - renaming columns
    data.columns = ['row_id', 'order_id', 'order_date', 'ship_date', 'ship_mode', 'customer_id', 'customer_name',
                    'segment', 'country', 'city', 'state', 'postal_code', 'region', 'product_id', 'category',
                    'sub_category', 'product_name', 'sales', 'quantity', 'discount', 'profit']

    # Convert order_date to datetime
    data['order_date'] = pd.to_datetime(data['order_date'])

    # Convert strings to numeric, replace unconvertable strings with '0'
    data['sales'] = pd.to_numeric(data['sales'], errors='coerce')
    data['sales'] = data.sales.fillna(0)

    # Sales can't be negative. Treating negatives as wrong data and replacing with 0
    data.loc[(data['sales'] < 0), 'sales'] = 0

    # Enrich the data by constructing 'product_url' column
    data['product_url'] = data.apply(lambda row: base_url + '/'.join([row.category, row.sub_category, row.product_id]),
                                     axis=1)

    # Reduce data by eliminating unused columns
    select_columns = ['customer_name', 'order_id', 'order_date', 'product_url', 'sales']
    data = data[select_columns]

    # Filter orders by date
    data = data[(data['order_date'] > start_date)]

    return data


def group_data(data):
    """
    Pre-process the data by grouping it by order_id
    """
    order_groups = {}

    for row in data.itertuples():
        if row.order_id not in order_groups:
            order_groups[row.order_id] = {'line_items': []}

        order_groups[row.order_id]['customer_name'] = row.customer_name
        order_groups[row.order_id]['order_date'] = f"{row.order_date:%Y-%m-%d}"
        order_groups[row.order_id]['order_id'] = row.order_id
        order_groups[row.order_id]['line_items'].append({
            'product_url': row.product_url,
            'revenue': row.sales
        })

    return order_groups


def groups_to_json(order_groups):
    """
    Convert grouped orders into dict with desired structure.
    """
    result = {}

    for order_id, order in order_groups.items():
        if order['customer_name'] not in result:
            result[order['customer_name']] = {'orders': []}

        result[order['customer_name']]['orders'].append(order)

    return result


@click.command()
@click.option('--base-url', default='https://www.foo.com',
              help='Base URL for JSON output. Default is "https://www.foo.com"')
@click.option('--start-date', default='2016-07-31',
              help='Start date to filter input TSV. Default is "2016-07-31"')
@click.argument('input', type=click.File('rb'), default='-')
@click.argument('output', type=click.File('w'), default='-', required=False)
def main(input, output, base_url, start_date):
    """This script transforms TSV file with orders into hierarchical JSON.
    Input and output files could be the standard output as denoted by the `-` sign.
        \n
        Process stdin to stdout:\n
            tsv2json.pl - -
        \n
        Process sampledata.tsv to stdout:\n
            tsv2json sampledata.tsv -
        \n
        Process sampledata.tsv and save to result.json:\n
            tsv2json sampledata.tsv result.json
    """
    tsv = read_tsv(input)
    data = process_data(tsv, base_url, start_date)
    groups = group_data(data)
    json_result = groups_to_json(groups)

    click.echo(json.dumps(json_result, indent=4, sort_keys=True),
               file=output)

if __name__ == "__main__":
    main()